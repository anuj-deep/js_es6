var msg = require("./greeting");
console.log(msg.Msg); // 'Hi'

msg.setMessage("Hello from greeting js");
console.log(msg.Msg); // 'Hi'

let a = "anuj";
console.log(a);
let b = [42, 100, 7, 3, 67, 33];
b.sort();
console.log(b); //[ 100, 3, 33, 42, 67, 7 ]
// sort numbers using comparator function
b.sort(function (a, b) {
	return a - b;
});
console.log(b); //[ 3, 7, 33, 42, 67, 100 ]

var x = 10;
// Here x is 10
{
	let x = 2;
	console.log(x);
	// Here x is 2
}
// Here x is 10
console.log(x);

//function expression
var f = function (x, y) {
	return x * y;
};
console.log(f(3, 8));

// ES6
const g = (x, y) => x * y;
console.log(g(7, 6));
// Arrow functions are not hoisted. They must be defined before they are used.

// Create Objects
const apples = { name: "Apples" };
const bananas = { name: "Bananas" };
const oranges = { name: "Oranges" };

// Create a new Map
const fruits = new Map();

// Add new Elements to the Map
fruits.set(apples, 500);
fruits.set(bananas, 300);
fruits.set(oranges, 200);

console.log(fruits);
console.log(fruits.get(bananas)); //300

// A JavaScript Set is a collection of unique values.

// Each value can only occur once in a Set.

// A Set can hold any value of any data type.

// Create a Set
const letters = new Set();

// Add Values to the Set
letters.add("a");
letters.add("b");
letters.add("c");
console.log(letters);

// JavaScript Classes are templates for JavaScript Objects.

// Use the keyword class to create a class.

// Always add a method named constructor()

class Car {
	constructor(name, year) {
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car("Ford", 2014);

console.log(myCar.name);

//Promises
// A Promise is an object representing the eventual completion or failure of an asynchronous operation.
//  a promise is an object that returns a value which you hope to receive in the future, but not now.
const promiseA = new Promise((resolve, reject) => {
	resolve(777);
	reject(500);
});
// At this point, "promiseA" is already settled.
promiseA
	.then((val) => {
		console.log("asynchronous logging has val1:", val); //777
		//if we don't return anything from here, then next then handler will run but it will get undefined
		// as value
		return val * 2;
	})
	.then((val) => {
		console.log("asynchronous logging has val2:", val); //1554
	})
	.catch((err) => {
		console.log("error is ", err);
	});
console.log("immediate logging");

// produces output in this order:
// immediate logging
// asynchronous logging has val: 777

//chaining after error
new Promise((resolve, reject) => {
	console.log("Initial");

	resolve();
})
	.then(() => {
		throw new Error("Something failed");

		console.log("Do this"); //it will not print
	})
	.catch(() => {
		console.error("first error handling"); //it will print
	})
	.then(() => {
		console.log("Do this, no matter what happened before"); //it will  print
	})
	.catch(() => {
		console.error("Do that"); //it will not print
	});

let learnJS = function makePromise(completed) {
	return new Promise(function (resolve, reject) {
		setTimeout(() => {
			if (completed) {
				resolve("I have completed learning JS.");
			} else {
				reject("I haven't completed learning JS yet.");
			}
		}, 3 * 1000);
	});
};

learnJS(true)
	.then((success) => console.log(success)) //it will  print
	.catch((reason) => console.log(reason)) //it will not print
	.finally(() => console.log("from finally")); //it will print

let xy = 5;
console.log(xy, " xy"); //if xy is accessed before declaration ,it will throw refrenceError
console.log(ab, " ab"); //it will print undefined
var ab = 10;

for (var i = 0; i < 5; i++) {
	setTimeout(function () {
		console.log(i + " from var setTimeout"); //will print 5 five times
	}, 1000);
}

for (let i = 0; i < 5; i++) {
	setTimeout(function () {
		console.log(i + " from let setTimeout"); // will print 0 to 4.
	}, 1000);
}
{
	// enter new scope, TDZ starts
	let log = function () {
		console.log(message); // messagedeclared later
	};

	// This is the TDZ and accessing log
	// would cause a ReferenceError

	let message = "Hello"; // TDZ ends(temporal dead zone)
	log(); // called outside TDZ
}
/*Variables are declared using the let keyword are block-scoped, are not initialized to any value,
 and are not attached to the global object.
Redeclaring a variable using the let keyword will cause an error.
A temporal dead zone of a variable declared using the let keyword starts from the block until the 
initialization is evaluated.*/

var counter = 220;
console.log(counter);

var ex = 150;
var ex;
console.log(ex); // 150
const RATE = 0.1;
// RATE = 0.2; /*TypeError */
console.log(RATE);
// const RED; /* SyntaxError*/
const person = { age: 20 };
person.age = 30; // OK
console.log(person.age); // 30

// Even though the person variable is a constant, you can change the value of its property.

// However, you cannot reassign a different value to the person constant like this:

// person = { age: 40 }; // TypeError

/*If you want the value of the person object to be immutable, you have to freeze it by using the
Object.freeze() method:

const person = Object.freeze({age: 20});
person.age = 30; // TypeError */

// default parameters
function say(message = "Hi") {
	console.log(message);
}

say(); // 'Hi'
say("Hello"); // 'Hello'
/*Sometimes, you can use the term argument and parameter interchangeably. However, by definition,
 parameters are what you specify in the function declaration whereas the arguments are
  what you pass to the function. */

function add(x = 3, y = x, z = x + y) {
	return x + y + z;
}

console.log(add()); // 12

/*ES6 provides a new kind of parameter so-called rest parameter that has a prefix of 
three dots (...). A rest parameter allows you to represent an indefinite number of
 arguments as an array. See the following syntax:

function fn(a,b,...args) {
   //...
}
Notice that the rest parameters must apper at the end of the argument list.
*/

function sum(...args) {
	return args
		.filter(function (e) {
			return typeof e === "number";
		})
		.reduce(function (prev, curr) {
			return prev + curr;
		});
}

let result = sum(103, "Hi", null, undefined, 204);
console.log(result); //307

const odd = [1, 3, 5];
const combined = [2, 4, 6, ...odd];
console.log(combined);
/*The rest parameters must be the last arguments of a function. However, the spread operator 
can be anywhere: */

var rivers = ["Nile", "Ganges", "Yangte"];
var moreRivers = ["Danube", "Amazon"];

// Array.prototype.push.apply(rivers, moreRivers);

rivers.push(...moreRivers);
console.log(rivers);

let prefix = "machine";
let machine = {
	[prefix + " name"]: "server",
	[prefix + " hours"]: 10000,
};

console.log(machine["machine name"]); // server
console.log(machine["machine hours"]); // 10000

// object destruction
const ratings = [
	{ user: "John", score: 3 },
	{ user: "Jane", score: 4 },
	{ user: "David", score: 5 },
	{ user: "Peter", score: 2 },
];

let sumx = 0;
for (const { score } of ratings) {
	sumx += score;
}

console.log(`Total scores: ${sumx}`); // 14

let w = 051;
console.log(w); // 41

let z = 0o51;
console.log(z); // 41

let e = parseInt("111", 2);
console.log(e); // 7

//Array destruction
function getScores() {
	return [70, 80, 90, 100];
}
let [xe, yr, ...args] = getScores();
console.log(xe); // 70
console.log(yr); // 80
console.log(args); // [90, 100]

// swapping values
let k = 10,
	l = 20;

[k, l] = [l, k];

console.log(k); // 20
console.log(l); // 10

let personX = {
	firstName: "John",
	lastName: "Doe",
	middleName: "C.",
	currentAge: 28,
};
// object destruction
// Object destructuring assigns the properties of an object to variables with the same names by default.
let { firstName, lastName, middleName = "", currentAge: age = 18 } = personX;

console.log(middleName); // 'C.'
console.log(age); // 28

/*
//sort.js
export default function(arr) {
  // sorting here
}
export function heapSort(arr) {
  // heapsort
}
//another js file
import sort, {heapSort} from './sort.js';
sort([2,1,3]);
heapSort([3,1,2]);
To import both default and non-default bindings, you use the specify a list of bindings after
the import keyword with the following rules:

The default binding must come first.
The non-default binding must be surrounded by curly braces.
 */

//  To verify the fact that classes are special "functions", you can use the typeof operator
// to check the type of the any class.

// First, class declarations are not hoisted like function declarations.

// Getters and Setters in class

class Student {
	constructor(name) {
		this.name = name;
	}
	//getters method
	get name() {
		return this._name;
	}
	//setters method
	set name(newName) {
		newName = newName.trim();
		if (newName === "") {
			throw "The name cannot be empty";
		}
		this._name = newName;
	}
}
let s1 = new Student("bittu");
console.log(s1.name); //bittu
s1.name = "rahul";
console.log(s1.name); //rahul

// computed property
let propName = "c";
const rank = {
	a: 1,
	b: 2,
	[propName]: 3,
};

console.log(rank.c, " computed property"); // 3

// JavaScript classes are first-class citizens. It means that you can pass a class into a function,
//  return it from a function, and assign it to a variable.
function factory(aClass) {
	return new aClass();
}

let greeting = factory(
	class {
		sayHi() {
			console.log("Hi from greeting class expression");
		}
	},
);

greeting.sayHi(); // 'Hi' from greeting class expression

/*
Singleton is a design pattern that limits the instantiation of a class to a single instance. It ensures
 that only one instance of a class can be created throughout the system.

"Class expressions" can be used to create a singleton by calling the class constructor immediately.

To do that, you use the new operator with a class expression and include the parentheses at the end of
 class declaration as shown in the following example:
*/
// singleton class
let app = new (class {
	constructor(name) {
		this.name = name;
	}
	start() {
		console.log(`Starting the ${this.name}...`);
	}
})("Awesome App");

app.start(); // Starting the Awesome App...

// Static methods
class Person {
	constructor(name) {
		this.name = name;
	}
	getName() {
		return this.name;
	}
	static createAnonymous(gender) {
		let name = gender == "male" ? "John Doe" : "Jane Doe";
		return new Person(name);
	}
}
console.log(Person.createAnonymous("male"));

//static variables

class Item {
	constructor(name, quantity) {
		this.name = name;
		this.quantity = quantity;
		this.constructor.count++;
	}
	static count = 0;
	static getCount() {
		return Item.count++;
	}
}

let pen = new Item("Pen", 5);
let notebook = new Item("notebook", 10);

console.log(Item.getCount()); // 2

// Class in JS

class Animal {
	constructor(legs) {
		this.legs = legs;
	}
	walk() {
		console.log("walking on " + this.legs + " legs");
	}
	static helloWorld() {
		console.log("Hello World from parent class");
	}
}

class Bird extends Animal {
	constructor(legs) {
		super(legs);
	}
	fly() {
		console.log("flying");
	}
	walk() {
		super.walk();
		console.log(`go walking`);
	}
}

let bird = new Bird(2);

bird.walk();
bird.fly();
Bird.helloWorld();

//arrow functions
/*
they are not hoisted like general function
Use the (...args) => expression; to define an arrow function.
Use the (...args) => { statements } to define an arrow function that has multiple statements.
An arrow function doesn’t have its binding to this or super.
An arrow function doesn’t have arguments object, new.target keyword, and prototype property.
*/
let names = ["John", "Mac", "Peter"];
let lengths = names.map((name) => name.length);
console.log(lengths);

// Function generator
/*
ES6 introduces a new kind of function that is different from a regular function: function generator
or generator.

A generator can pause midway and then continues from where it paused. For example:

*/
function* generate() {
	console.log("invoked 1st time");
	yield 1;
	console.log("invoked 2nd time");
	yield 2;
}
/*
Let’s examine the generate() function in detail.

First, you see the asterisk (*) after the function keyword. The asterisk denotes that the generate() 
is a generator, not a normal function.
Second, the yield statement returns a value and pauses the execution of the function.
*/
let gen = generate();
console.log(gen); //Object [Generator] {}

let result1 = gen.next();
console.log(result1); //invoked 1st time  //{ value: 1, done: false }
/*
As you can see, the Generator object executes its body which outputs message 'invoked 1st time' at 
line 1 and returns the value 1 at line 2.

The yield statement returns 1 and pauses the generator at line 2.
*/
result1 = gen.next(); //invoked 1st time
console.log(result1); //invoked 2nd time
/*
invoked 2nd time
{ value: 2, done: false }
*/
function* forever() {
	let index = 0;
	while (true) {
		yield index++;
	}
}

let fg = forever();
console.log(fg.next(), " invoked 1st time"); // 0
console.log(fg.next(), " invoked 2nd time"); // 1
console.log(fg.next(), " invoked 3rd time"); // 2
console.log(fg.next(), " invoked 4th time"); //3

let chars = Array.of("A", "B", "C");
console.log(chars.length); // 3
console.log(chars); // ['A','B','C']

// find
/* 
The find() executes the callback function for each element in the array until the callback returns 
a truthy value. 

If the callback returns a truthy value, the find() immediately returns the element and stop searching.
Otherwise, it returns undefined.

If you want to find the index of the found element, you can use the findIndex() method.
*/
let customers = [
	{
		name: "ABC Inc",
		credit: 100,
	},
	{
		name: "ACME Corp",
		credit: 200,
	},
	{
		name: "IoT AG",
		credit: 300,
	},
];

console.log(customers.find((c) => c.credit > 100)); //{ name: 'ACME Corp', credit: 200 }
// find index
let ranks = [1, 5, 7, 8, 10, 7];
let index = ranks.findIndex((rank) => rank === 7);
console.log(index, " from findIndex"); //2  from findIndex

// object assign for copying object
let widget = {
	color: "red",
};

let clonedWidget = Object.assign({}, widget);

console.log(clonedWidget); //{ color: 'red' }
// object assign for merging objects
let box = {
	height: 10,
	width: 20,
	color: "Red",
};

let style = {
	color: "Blue",
	borderStyle: "solid",
};

let styleBox = Object.assign({}, box, style);

console.log(styleBox); //{ height: 10, width: 20, color: 'Blue', borderStyle: 'solid' }

let quantity = NaN;
console.log(quantity === quantity); //false
console.log(typeof quantity); //number
console.log(undefined == undefined); //true
console.log(null == null); //true
console.log(15 == "15"); //true
console.log(undefined == null); //true
console.log(undefined === null); //false

// string includes

let str = "JavaScript String";
console.log(str.includes("Script")); //false

// starts with
const title = "Jack and Jill Went Up the Hill";

console.log(title.startsWith("Jack")); //true
console.log(title.startsWith("jack")); //false
console.log(title.startsWith("Jill", 9)); //true
